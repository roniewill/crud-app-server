const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
app.use(cors());

const schema = buildSchema(`
  type Cliente {
    id: Int
    nome: String
    sobrenome: String
    email: String
    telefone: String
    idade: Int
  }
  type Query {
    getClientes: [Cliente],
    getClienteInfo(id: Int) : Cliente
  }
  type Mutation {
    createCliente(nome: String, sobrenome: String, email: String, telefone: String, idade: Int) : Boolean
    updateClienteInfo(id: Int, nome: String, sobrenome: String, email: String, telefone: String, idade: Int) : Boolean
    deleteCliente(id: Int) : Boolean
  }
`);

const queryDB = (req, sql, args) =>
  new Promise((resolve, reject) => {
    req.mysqlDb.query(sql, args, (err, rows) => {
      if (err) return reject(err);
      rows.changedRows || rows.affectedRows || rows.insertId
        ? resolve(true)
        : resolve(rows);
    });
  });

const root = {
  getClientes: (args, req) =>
    queryDB(req, 'select * from clientes order by id desc').then(data => {
      //console.log('data ', data);
      return data;
    }),
  getClienteInfo: (args, req) =>
    queryDB(req, 'select * from clientes where id = ?', [args.id]).then(
      data => data[0]
    ),
  updateClienteInfo: (args, req) =>
    queryDB(req, 'update clientes SET ? where id = ?', [args, args.id]).then(
      data => data
    ),
  createCliente: (args, req) =>
    queryDB(req, 'insert into clientes SET ?', args).then(data => data),
  deleteCliente: (args, req) =>
    queryDB(req, 'delete from clientes where id = ?', [args.id]).then(
      data => data
    ),
};

app.use((req, res, next) => {
  req.mysqlDb = mysql.createConnection({
    host: 'app-crud.ct1wtoy4tjcj.us-east-1.rds.amazonaws.com',
    user: 'admin',
    password: 'TzXGHyIGKg',
    database: 'crud',
  });
  req.mysqlDb.connect();
  next();
});

app.use(
  '/graphql',
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

app.listen({ port: process.env.PORT || 4000 });

console.log('Running a GraphQL API server at localhost:4000/graphql');
